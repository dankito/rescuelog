package net.codinux.medical.rescuelog.model

import junit.framework.TestCase
import java.util.*

class ResuscitationLogTest : TestCase() {

    fun testElapsed() {
        val log = ResuscitationLog(1, Date())
        Thread.sleep(3000)
        assertEquals("00:03", log.elapsed())
    }
}