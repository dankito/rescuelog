package net.codinux.medical.rescuelog.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import kotlinx.android.synthetic.main.activity_code.*
import net.codinux.medical.rescuelog.R
import net.codinux.medical.rescuelog.presenter.ResuscitationPresenter
import org.koin.android.ext.android.inject
import java.util.*
import kotlin.concurrent.scheduleAtFixedRate


class CodeActivity : AppCompatActivity() {

    val presenter : ResuscitationPresenter by inject()
    var updateElapsedTimeTimer : Timer = Timer()

    fun engageScreenAlwaysOn() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }
    fun disengageScreenAlwaysOn() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_code)
        engageScreenAlwaysOn()
        presenter.startResuscitation()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        rhythm.setOnClickListener {
            presenter.clickedButtonRhythm()
        }

        shock.setOnClickListener {
            presenter.clickedButtonShock()
        }

        adrenalin.setOnClickListener {
            presenter.clickedButtonAdrenalin()
        }

        amiodaron.setOnClickListener {
            presenter.clickedButtonAmiodaron()
        }

        ioiv.setOnClickListener {
            presenter.clickedButtonIoiv()
        }

        airway.setOnClickListener {
            presenter.clickedButtonAirway()
        }

        lucas.setOnClickListener {
            presenter.clickedButtonLucas()
        }

        record.setOnClickListener {
            presenter.clickedButtonRecord()
        }

        rosc.setOnClickListener {
            presenter.finishResusciation()
            disengageScreenAlwaysOn()
            finish()
        }

        updateElapsedTimeTimer.scheduleAtFixedRate(500L, 500L) {
            runOnUiThread { total_time.text = presenter.elapsedResuscitationTime() }
        }
    }
}