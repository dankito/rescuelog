package net.codinux.medical.rescuelog.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.core.widget.doOnTextChanged
import kotlinx.android.synthetic.main.activity_settings.*
import net.codinux.medical.rescuelog.R
import net.codinux.medical.rescuelog.presenter.ResuscitationPresenter
import org.koin.android.ext.android.inject


class SettingsActivity : AppCompatActivity() {

    private val presenter: ResuscitationPresenter by inject()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_settings)
    }

    fun loadSettings() {
        rhythmAnalysisTimerInSeconds.setText(presenter.appSettings.rhythmAnalysisTimerInSeconds.toString())
        shockTimerInSeconds.setText(presenter.appSettings.shockTimerInSeconds.toString())
        adrenalinTimerInSeconds.setText(presenter.appSettings.adrenalinTimerInSeconds.toString())
        informUserCountSecondsBeforeTimerCountDown.setText(presenter.appSettings.informUserCountSecondsBeforeTimerCountDown.toString())
        informUserOfTimerCountDownOptically.isChecked =
            presenter.appSettings.informUserOfTimerCountDownOptically
        informUserOfTimerCountDownWithSound.isChecked =
            presenter.appSettings.informUserOfTimerCountDownWithSound
        startRecordingAudioAtStart.isChecked = presenter.appSettings.startRecordingAudioAtStart
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        loadSettings()

        backButton.setOnClickListener {
            finish()
        }

        rhythmAnalysisTimerInSeconds.doAfterTextChanged { textField ->
            if (!textField.isNullOrBlank()) {
                presenter.appSettings.rhythmAnalysisTimerInSeconds = textField.toString().toInt()
                presenter.saveAppSettings(presenter.appSettings)
            }
        }

        shockTimerInSeconds.doAfterTextChanged { textField ->
            if (!textField.isNullOrBlank()) {
                presenter.appSettings.shockTimerInSeconds = textField.toString().toInt()
                presenter.saveAppSettings(presenter.appSettings)
            }
        }

        adrenalinTimerInSeconds.doAfterTextChanged { textField ->
            if (!textField.isNullOrBlank()) {
                presenter.appSettings.adrenalinTimerInSeconds = textField.toString().toInt()
                presenter.saveAppSettings(presenter.appSettings)
            }
        }

        informUserCountSecondsBeforeTimerCountDown.doAfterTextChanged { textField ->
            if (!textField.isNullOrBlank()) {
                presenter.appSettings.informUserCountSecondsBeforeTimerCountDown =
                    textField.toString().toInt()
                presenter.saveAppSettings(presenter.appSettings)
            }
        }

        informUserOfTimerCountDownOptically.setOnCheckedChangeListener { buttonview, isChecked ->
            presenter.appSettings.informUserOfTimerCountDownOptically = isChecked
            presenter.saveAppSettings(presenter.appSettings)
        }

        informUserOfTimerCountDownWithSound.setOnCheckedChangeListener { buttonview, isChecked ->
            presenter.appSettings.informUserOfTimerCountDownWithSound = isChecked
            presenter.saveAppSettings(presenter.appSettings)
        }

        startRecordingAudioAtStart.setOnCheckedChangeListener { buttonview, isChecked ->
            presenter.appSettings.startRecordingAudioAtStart = isChecked
            presenter.saveAppSettings(presenter.appSettings)
        }

    }

}


