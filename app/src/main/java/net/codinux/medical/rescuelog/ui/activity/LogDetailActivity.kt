package net.codinux.medical.rescuelog.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import kotlinx.android.synthetic.main.activity_log_detail.*
import net.codinux.medical.rescuelog.R
import net.codinux.medical.rescuelog.model.ResuscitationLog
import net.codinux.medical.rescuelog.presenter.ResuscitationPresenter
import net.codinux.medical.rescuelog.ui.adapter.LogEntryItem
import org.koin.android.ext.android.inject


class LogDetailActivity : AppCompatActivity() {

    companion object {

        const val ResuscitationLogIdParameterName = "ResuscitationLogId"

    }


    private lateinit var resuscitationLog: ResuscitationLog

    private val presenter: ResuscitationPresenter by inject()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_log_detail)

        intent?.getLongExtra(ResuscitationLogIdParameterName, -1)?.let { resuscitationLogId ->
            presenter.getResuscitationLog(resuscitationLogId)?.let {
                resuscitationLog = it

                txtStartDate.text = presenter.formatDate(resuscitationLog.startTime)
                txtStartTime.text = presenter.formatTimeWithSeconds(resuscitationLog.startTime)

                val logEntryAdapter = ItemAdapter<LogEntryItem>()
                val fastAdapter = FastAdapter.with(logEntryAdapter)

                rcyLogEntries.layoutManager = LinearLayoutManager(this)
                rcyLogEntries.itemAnimator = DefaultItemAnimator()
                rcyLogEntries.adapter = fastAdapter

                logEntryAdapter.add(resuscitationLog.logEntries.map { LogEntryItem(it, presenter) })
            }
        }
    }

}