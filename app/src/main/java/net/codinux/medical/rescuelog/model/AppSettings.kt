package net.codinux.medical.rescuelog.model

class AppSettings {
    var rhythmAnalysisTimerInSeconds: Int = 0
    var shockTimerInSeconds: Int = 0
    var adrenalinTimerInSeconds: Int = 0
    var informUserCountSecondsBeforeTimerCountDown: Int = 0
    var informUserOfTimerCountDownOptically: Boolean = false
    var informUserOfTimerCountDownWithSound: Boolean = false
    var startRecordingAudioAtStart: Boolean = false
}