package net.codinux.medical.rescuelog.extensions

import android.app.Activity
import android.content.Context
import android.content.Intent


fun <T : Activity> Context.navigateToActivity(activityClass: Class<T>, parameterName: String? = null, parameter: Any? = null) {
    val intent = Intent(applicationContext, activityClass)

    parameterName?.let {
        when {
            parameter is String -> intent.putExtra(parameterName, parameter)
            parameter is Int -> intent.putExtra(parameterName, parameter)
            parameter is Long -> intent.putExtra(parameterName, parameter)
            else -> { } // to make compiler happy
        }
    }

    startActivity(intent)
}