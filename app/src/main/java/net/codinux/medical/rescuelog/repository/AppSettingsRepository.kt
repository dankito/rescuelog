package net.codinux.medical.rescuelog.repository

import net.codinux.medical.rescuelog.model.AppSettings

interface AppSettingsRepository {
    fun get(): AppSettings
    fun save(appSettings: AppSettings)
}