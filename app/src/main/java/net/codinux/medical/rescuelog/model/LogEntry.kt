package net.codinux.medical.rescuelog.model

import java.util.*

class LogEntry(
    val id: Long = 0,
    val timestamp: Date = Date(),
    val action: ResuscitationAction
) {

    constructor() : this(0, Date(), ResuscitationAction.RHYTHM_ANALYSIS)

}