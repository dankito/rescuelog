package net.codinux.medical.rescuelog.repository

interface ResuscitationRepository : AppSettingsRepository, ResuscitationLogRepository {
}