package net.codinux.medical.rescuelog.repository

import net.codinux.medical.rescuelog.model.ResuscitationLog

interface ResuscitationLogRepository {
    fun nextResuscitationLogId() : Long
    fun save(log: ResuscitationLog) : ResuscitationLog
    fun find(id: Long) : ResuscitationLog?
    fun findAll() : List<ResuscitationLog>
    fun delete(log: ResuscitationLog)
}