package net.codinux.medical.rescuelog.presenter

import net.codinux.medical.rescuelog.model.AppSettings
import net.codinux.medical.rescuelog.model.LogEntry
import net.codinux.medical.rescuelog.model.ResuscitationAction
import net.codinux.medical.rescuelog.model.ResuscitationLog
import net.codinux.medical.rescuelog.repository.ResuscitationRepository
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class ResuscitationPresenter(val resuscitationRepository: ResuscitationRepository) {

    companion object {

        val LogDateFormat = DateFormat.getDateInstance(DateFormat.LONG) // prefixed with 'Log' to avoid naming conflict with DateFormatter

        val TimeFormat = SimpleDateFormat("HH:mm")

        val TimeWithSecondsFormat = SimpleDateFormat("HH:mm:ss")

    }


    var appSettings: AppSettings = AppSettings()
    var log: ResuscitationLog = ResuscitationLog()
    var isRecording: Boolean = false

    fun saveAppSettings(appSettings: AppSettings) {
        this.resuscitationRepository.save(appSettings)
    }

    fun startResuscitation(): ResuscitationLog {
        log = ResuscitationLog()
        log.startTime = Date()
        log = resuscitationRepository.save(log)
        return log
    }

    fun finishResusciation(): ResuscitationLog {
        log.endTime = Date()
        log = resuscitationRepository.save(log)
        return log
    }

    fun saveResuscitationLog(log: ResuscitationLog): ResuscitationLog {
        resuscitationRepository.save(log)
        return log
    }

    fun addLogEntry(log: ResuscitationLog, time: Date, action: ResuscitationAction) {
        val logEntryId = (log.logEntries.size + 1).toLong()
        val logEntry = LogEntry(logEntryId, time, action)
        log.logEntries.add(logEntry)
        resuscitationRepository.save(log)
    }

    fun resuscitationStopped(log: ResuscitationLog) {
        log.endTime = Date()
        resuscitationRepository.save(log)
    }

    fun deleteResuscitationLog(log: ResuscitationLog) {
        resuscitationRepository.delete(log)
    }

    fun getResuscitationLogs(): List<ResuscitationLog> {
        return resuscitationRepository.findAll()
    }

    fun getResuscitationLog(logId: Long): ResuscitationLog? {
        return resuscitationRepository.find(logId)
    }

    fun clickedButtonRhythm() {
        log.addLogEntry(ResuscitationAction.RHYTHM_ANALYSIS)
        log = resuscitationRepository.save(log)
    }

    fun clickedButtonShock() {
        log.addLogEntry(ResuscitationAction.SHOCK)
        log = resuscitationRepository.save(log)
    }

    fun clickedButtonAdrenalin() {
        log.addLogEntry(ResuscitationAction.ADRENALIN)
        log = resuscitationRepository.save(log)
    }

    fun clickedButtonAmiodaron() {
        log.addLogEntry(ResuscitationAction.AMIODARON)
        log = resuscitationRepository.save(log)
    }

    fun clickedButtonIoiv() {
        log.addLogEntry(ResuscitationAction.IO_IV)
        log = resuscitationRepository.save(log)
    }

    fun clickedButtonAirway() {
        log.addLogEntry(ResuscitationAction.AIRWAY)
        log = resuscitationRepository.save(log)
    }

    fun clickedButtonLucas() {
        log.addLogEntry(ResuscitationAction.LUCAS)
        log = resuscitationRepository.save(log)
    }

    fun clickedButtonRecord() {
        if (!isRecording) {
            // TODO: start recording
        } else {
            // TODO: stop recording
        }
        log = resuscitationRepository.save(log)
    }

    fun elapsedResuscitationTime(): String {
        return log.elapsed()
    }

//
//
//    fun getAudioPath(audioFilename: String): URL?
//
//
//    fun exportLogAsString(log: ResuscitationLog): String
//
//    fun convertLogToCsv(log: ResuscitationLog): String
//
//
//    fun translateLogEntryType(typeInt: Int32): String


    fun formatDate(date: Date): String {
        return LogDateFormat.format(date)
    }

    fun formatTime(time: Date): String {
        return TimeFormat.format(time)
    }

    fun formatTimeWithSeconds(time: Date): String {
        return TimeWithSecondsFormat.format(time)
    }

//    fun formatDateTime(date: Date?) -> String
//
//    fun formatDuration(startTime: Date, countDecimalPlacesForMinutes: Int = 1): String
//
//    fun formatDuration(seconds: Int, countDecimalPlacesForMinutes: Int = 1, displayAsNegativeNumber: Bool = false): String
//
//    fun formatDuration(seconds: Double, countDecimalPlacesForMinutes: Int = 1, displayAsNegativeNumber: Bool = false): String
//
//
//    fun preventScreenLock()
//
//    fun reenableScreenLock()

}