package net.codinux.medical.rescuelog.model

enum class ResuscitationAction {
    RHYTHM_ANALYSIS,
    SHOCK,
    ADRENALIN,
    AMIODARON,
    IO_IV,
    AIRWAY,
    LUCAS
}