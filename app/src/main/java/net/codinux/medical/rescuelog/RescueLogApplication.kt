package net.codinux.medical.rescuelog

import androidx.multidex.MultiDexApplication
import net.codinux.medical.rescuelog.presenter.ResuscitationPresenter
import net.codinux.medical.rescuelog.repository.ResuscitationJsonRepository
import net.codinux.medical.rescuelog.repository.ResuscitationRepository
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module
import java.io.File


class RescueLogApplication : MultiDexApplication() {

    override fun onCreate(){
        super.onCreate()

        setupDI()
    }

    private fun setupDI() {
        startKoin {
            // Android context
            androidContext(this@RescueLogApplication)
            // modules
            modules(module)
        }
    }


    val module = module {
        single<ResuscitationRepository> { ResuscitationJsonRepository(File(applicationContext.filesDir, "data")) }
        single { ResuscitationPresenter(get()) }
    }

}