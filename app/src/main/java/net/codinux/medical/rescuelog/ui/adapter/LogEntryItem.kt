package net.codinux.medical.rescuelog.ui.adapter

import android.content.Context
import android.view.View
import android.widget.TextView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import net.codinux.medical.rescuelog.R
import net.codinux.medical.rescuelog.model.LogEntry
import net.codinux.medical.rescuelog.model.ResuscitationAction
import net.codinux.medical.rescuelog.presenter.ResuscitationPresenter


class LogEntryItem(
    val logEntry: LogEntry,
    val presenter: ResuscitationPresenter
) : AbstractItem<LogEntryItem.ViewHolder>() {

    override val layoutRes: Int
        get() = R.layout.list_item_log_entry

    override val type: Int
        get() = R.id.log_entry_item_id

    override fun getViewHolder(view: View): ViewHolder {
        return ViewHolder(view)
    }


    class ViewHolder(view: View) : FastAdapter.ViewHolder<LogEntryItem>(view) {

        var txtResuscitationLogAction: TextView = view.findViewById(R.id.txtResuscitationLogAction)

        var txtResuscitationLogTimestamp: TextView = view.findViewById(R.id.txtResuscitationLogTimestamp)


        override fun bindView(item: LogEntryItem, payloads: List<Any>) {
            val logEntry = item.logEntry
            val presenter = item.presenter

            txtResuscitationLogAction.text = translate(txtResuscitationLogAction.context, logEntry.action)
            txtResuscitationLogTimestamp.text = presenter.formatTimeWithSeconds(logEntry.timestamp)
        }

        override fun unbindView(item: LogEntryItem) {

        }

        private fun translate(context: Context, action: ResuscitationAction): String {
            return context.getString(getLocalizationKey(action))
        }

        private fun getLocalizationKey(action: ResuscitationAction): Int {
            return when (action) {
                ResuscitationAction.RHYTHM_ANALYSIS -> R.string.resuscitation_action_rhythm_analysis
                ResuscitationAction.SHOCK -> R.string.resuscitation_action_shock
                ResuscitationAction.ADRENALIN -> R.string.resuscitation_action_adrenalin
                ResuscitationAction.AMIODARON -> R.string.resuscitation_action_amiodaron
                ResuscitationAction.IO_IV -> R.string.resuscitation_action_io_iv
                ResuscitationAction.AIRWAY -> R.string.resuscitation_action_airway
                ResuscitationAction.LUCAS -> R.string.resuscitation_action_lucas
            }
        }

    }
}