package net.codinux.medical.rescuelog

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import net.codinux.medical.rescuelog.extensions.navigateToActivity
import net.codinux.medical.rescuelog.ui.activity.CodeActivity
import kotlinx.android.synthetic.main.activity_main.*
import net.codinux.medical.rescuelog.ui.activity.LogsOverviewActivity
import net.codinux.medical.rescuelog.ui.activity.SettingsActivity


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        btnNavigateToCodeActivity.setOnClickListener { navigateToActivity(CodeActivity::class.java) }
        btnNavigateToSettingsActivity.setOnClickListener { navigateToActivity(SettingsActivity::class.java) }
        btnNavigateToLogsOverviewActivity.setOnClickListener { navigateToActivity(LogsOverviewActivity::class.java) }
    }

}