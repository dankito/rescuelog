package net.codinux.medical.rescuelog.model

import java.text.SimpleDateFormat
import java.util.*

/**
 * Log einer Wiederbelebungsmassnahme
 */
data class ResuscitationLog(
    var id: Long = UnpersistedLogId,
    var startTime: Date = Date(),
    var endTime: Date = Date(),
    var audioFilename: String = "",
    var logEntries: MutableList<LogEntry> = mutableListOf()
) {

    companion object {
        const val UnpersistedLogId = -1L
    }


    fun elapsed(): String {
        val diff = Date(Date().getTime() - startTime.getTime());
        return SimpleDateFormat("mm:ss").format(diff)
    }

    fun addLogEntry(action : ResuscitationAction) {
        logEntries.add(LogEntry(action = action))
    }
}