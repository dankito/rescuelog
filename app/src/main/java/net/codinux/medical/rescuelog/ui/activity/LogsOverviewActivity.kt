package net.codinux.medical.rescuelog.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import kotlinx.android.synthetic.main.activity_logs_overview.*
import net.codinux.medical.rescuelog.R
import net.codinux.medical.rescuelog.extensions.navigateToActivity
import net.codinux.medical.rescuelog.presenter.ResuscitationPresenter
import net.codinux.medical.rescuelog.ui.adapter.ResuscitationLogItem
import org.koin.android.ext.android.inject


class LogsOverviewActivity : AppCompatActivity() {

    private val presenter: ResuscitationPresenter by inject()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_logs_overview)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        val resuscitationLogAdapter = ItemAdapter<ResuscitationLogItem>()
        val fastAdapter = FastAdapter.with(resuscitationLogAdapter)

        rcyResuscitationLogs.layoutManager = LinearLayoutManager(this)
        rcyResuscitationLogs.itemAnimator = DefaultItemAnimator()
        rcyResuscitationLogs.adapter = fastAdapter

        fastAdapter.onClickListener = { _, _, item, _ ->
            navigateToActivity(LogDetailActivity::class.java, LogDetailActivity.ResuscitationLogIdParameterName, item.log.id)
            true
        }

        val logs = presenter.getResuscitationLogs()

        resuscitationLogAdapter.add(logs.map { ResuscitationLogItem(it, presenter) })
    }

}