package net.codinux.medical.rescuelog.ui.adapter

import android.view.View
import android.widget.TextView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import net.codinux.medical.rescuelog.R
import net.codinux.medical.rescuelog.model.ResuscitationLog
import net.codinux.medical.rescuelog.presenter.ResuscitationPresenter


class ResuscitationLogItem(
    val log: ResuscitationLog,
    val presenter: ResuscitationPresenter
) : AbstractItem<ResuscitationLogItem.ViewHolder>() {


    override val layoutRes: Int
        get() = R.layout.list_item_resuscitation_log

    override val type: Int
        get() = R.id.resuscitation_log_item_id

    override fun getViewHolder(view: View): ViewHolder {
        return ViewHolder(view)
    }


    class ViewHolder(view: View) : FastAdapter.ViewHolder<ResuscitationLogItem>(view) {

        var txtResuscitationLogDate: TextView = view.findViewById(R.id.txtResuscitationLogDate)

        var txtResuscitationLogTime: TextView = view.findViewById(R.id.txtResuscitationLogTime)


        override fun bindView(item: ResuscitationLogItem, payloads: List<Any>) {
            val log = item.log
            val presenter = item.presenter

            txtResuscitationLogDate.text = presenter.formatDate(log.startTime)
            txtResuscitationLogTime.text = presenter.formatTime(log.startTime)
        }

        override fun unbindView(item: ResuscitationLogItem) {
        }

    }
}