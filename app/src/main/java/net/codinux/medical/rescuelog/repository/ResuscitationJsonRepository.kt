package net.codinux.medical.rescuelog.repository

import net.codinux.medical.rescuelog.model.AppSettings
import net.codinux.medical.rescuelog.model.ResuscitationLog
import net.dankito.utils.serialization.ISerializer
import net.dankito.utils.serialization.JacksonJsonSerializer
import java.io.File


class ResuscitationJsonRepository(
    protected val databaseFolder: File,
    protected val serializer: ISerializer = JacksonJsonSerializer()
) : ResuscitationRepository {

    companion object {
        const val ResuscitationLogJsonFileName = "resuscitation_log.json"

        const val AppSettingsJsonFileName = "app_settings.json"
    }


    protected val resuscitationLogJsonFile: File

    protected val appSettingsJsonFile: File

    protected var allResuscitationLogs = mutableListOf<ResuscitationLog>()


    init {
        databaseFolder.mkdirs()

        resuscitationLogJsonFile = File(databaseFolder, ResuscitationLogJsonFileName)
        appSettingsJsonFile = File(databaseFolder, AppSettingsJsonFileName)
    }


    override fun get(): AppSettings {
        var settings = serializer.deserializeObject(appSettingsJsonFile, AppSettings::class.java)

        if(settings == null) {
            throw DeserializationException("could not deserialize AppSettings")
        }

        return settings
    }

    override fun save(appSettings: AppSettings) {
        serializer.serializeObject(appSettings, appSettingsJsonFile)
    }


    override fun save(log: ResuscitationLog): ResuscitationLog {
        if (log.id == ResuscitationLog.UnpersistedLogId) {
            log.id = nextResuscitationLogId()
            (allResuscitationLogs as? MutableList<ResuscitationLog>)?.add(log)
        }

        saveAllLogs(allResuscitationLogs)

        return log
    }

    override fun nextResuscitationLogId(): Long {
        return (allResuscitationLogs?.size ?: 0) + 1L
    }

    override fun find(id: Long): ResuscitationLog? {
        return allResuscitationLogs?.firstOrNull { it.id == id }
    }

    override fun findAll(): List<ResuscitationLog> {
        val allLogs = serializer.deserializeListOr(resuscitationLogJsonFile, ResuscitationLog::class.java, mutableListOf()).toMutableList()

        this.allResuscitationLogs = allLogs

        return allLogs
    }

    override fun delete(log: ResuscitationLog) {
        (allResuscitationLogs as? MutableList<ResuscitationLog>)?.remove(log)

        saveAllLogs(allResuscitationLogs)
    }

    private fun saveAllLogs(allLogs: MutableList<ResuscitationLog>) {
        this.allResuscitationLogs = allLogs

        serializer.serializeObject(allLogs, resuscitationLogJsonFile)
    }

}