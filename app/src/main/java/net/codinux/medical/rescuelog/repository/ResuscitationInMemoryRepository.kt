package net.codinux.medical.rescuelog.repository

import net.codinux.medical.rescuelog.model.AppSettings
import net.codinux.medical.rescuelog.model.ResuscitationLog


class ResuscitationInMemoryRepository : ResuscitationRepository {

    var appSettings : AppSettings = AppSettings();
    var lastId : Long = 0;
    var resuscitationLogs : MutableMap<Long, ResuscitationLog> = mutableMapOf();

    override fun get(): AppSettings {
        return appSettings
    }

    override fun save(appSettings: AppSettings) {
        this.appSettings = appSettings
    }

    override fun nextResuscitationLogId(): Long {
        lastId = lastId + 1
        return lastId
    }

    override fun save(log: ResuscitationLog): ResuscitationLog {
        if(log.id == 0L) {
            log.id = nextResuscitationLogId()
        }
        resuscitationLogs.put(log.id, log);
        return log
    }

    override fun findAll(): List<ResuscitationLog> {
        return resuscitationLogs.values.toList().sortedBy { log -> log.startTime }.reversed();
    }

    override fun find(id: Long): ResuscitationLog? {
        return resuscitationLogs.get(id)
    }

    override fun delete(log: ResuscitationLog) {
        resuscitationLogs.remove(log.id)
    }


}