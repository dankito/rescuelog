package net.codinux.medical.rescuelog.repository

import java.lang.RuntimeException

class DeserializationException(message: String?) : Throwable(message) {
}